Статті та нариси

1. <a href="https://axiosis.gitlab.io/articles/anders/anders.pdf">Модальна гомотопічна мова математики</a>
2. <a href="https://axiosis.gitlab.io/articles/henk/pts_ua.pdf">Система доведення теорем з однiєю аксiомою</a>
3. <a href="https://axiosis.gitlab.io/articles/per/mltt.pdf">Мінімальна система для вбудовування MLTT</a> (<a href="https://axiosis.gitlab.io/articles/per/anno_ua.pdf">Анотація</a>)
4. <a href="https://axiosis.gitlab.io/articles/bloch/quantum.pdf">Мова для квантових комп'ютерів</a>

